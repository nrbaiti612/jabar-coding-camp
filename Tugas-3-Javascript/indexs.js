// soal no 1
var pertama = "Saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var satu = pertama.substring(0 , 4);
var dua = pertama.substr(11 , 7);
var tiga = kedua.substring(0 , 18);

// jawaban no 1
console.log(satu , dua , tiga);

// soal no 2
var a = 10;
var b = 2;
var c = 4;
var d = 6;

// jawaban no 2
console.log(a-c)*(d-b);

// soal no 3 
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(4,10);
var kataKetiga = kalimat.substr(14,4); 
var kataKeempat = kalimat.substr(19,6);
var kataKelima = kalimat.substr(24,7); 

// jawaban no 3
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

