// no 1
var Hewan = [];
Hewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
console.log(Hewan[4]);
console.log(Hewan[0]);
console.log(Hewan[2]);
console.log(Hewan[3]);
console.log(Hewan[1]);

// no 2

function introduce(){
    console.log("Nama saya " [name], "umur saya" [age] , "alamat saya di" [address], "dan saya punya hobby " yaitu [hobby]);
}
var data = {name : "Nurbaiti" , age : "19" , address : "Pancasan Bawah" , hobby : "Rebahan" };
 
var perkenalan = introduce(data);
console.log(perkenalan);

// no 3

function hitung_huruf_vokal(str){
    const count = str.match(/[aeiou]/gi).length;
    return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2);